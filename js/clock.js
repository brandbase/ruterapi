Number.prototype.pad = function (n,str){
  return Array(n-String(this).length+1).join(str||'0')+this;
}
String.prototype.pad = function(n, str){
  return Array(n-String(this).length+1).join(str||'0')+this;
}

var showRGB = false;

function updateTime(){
  var date = new Date(),
      hours = date.getHours(),
      minutes = date.getMinutes(),
      seconds = date.getSeconds(),
      red = Math.round((255/23) * hours),
      green = Math.round((255/59) * minutes),
      blue = Math.round((255/59) * seconds),
      color = 'rgb('+red+','+green+','+blue+')';
  $(".hour").text(hours.pad(2));
  $(".minute").text(minutes.pad(2));
  $(".second").text(seconds.pad(2));
  //console.log(color);
  if(showRGB){
    $('.code').text(color);
  } else {
    $('.code').text('#'+red.toString(16).pad(2)+green.toString(16).pad(2)+blue.toString(16).pad(2));
  }
  if(red > 200 && green > 200 && blue > 200){
    $(".clock").addClass("light");
  } else {
    $(".clock").removeClass("light");
  }
  //$('body').css('background',color);
}



setInterval(updateTime,1000);
updateTime();