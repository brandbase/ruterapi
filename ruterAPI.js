angular.module('myBusStopApp', [])

.filter('fromNow', function() {
        return function(dateString) {
            moment.lang('nb'); 
            return moment(dateString).fromNow();
        };
 })
 .filter('Arrival', function() {
		return function(dateString) {
			moment.lang('nb');
			return moment(dateString).format("HH:mm");
		};
})



.controller('myBusStopCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.realTimeData;

    var url = "http://reis.trafikanten.no/reisrest/realtime/getrealtimedata/2170018" + "?callback=JSON_CALLBACK";

    $http.jsonp(url)
        .success(function(data){
            $scope.realTimeData = data;
        });
		
	$scope.quantity = 5;
}])


.controller('myTrainStopCtrl', ['$scope', '$http', function ($scope, $http) {
	$scope.realTimeData;
	
	var url = "http://reis.trafikanten.no/reisrest/realtime/getrealtimedata/2170100" + "?callback=JSON_CALLBACK";

    $http.jsonp(url)
        .success(function(data){
            $scope.realTimeData = data;
        });
	$scope.quantity = 5;
}]);
	